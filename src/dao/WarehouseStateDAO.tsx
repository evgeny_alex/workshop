import { PictureWarehouse } from '../components/workshop/PictureWarehouse';
import WarehouseState from '../components/warehouse/WarehouseState';

export class WarehouseStateDAO {
  static addPicture(pictureWarehouse: PictureWarehouse, totalSize: number) {
    let warehouseState: WarehouseState = { currentSize: 0, picturesWarehouse: [] };

    let warehouseStateJSON = sessionStorage.getItem('warehouse-state');

    if (warehouseStateJSON !== null) {
      warehouseState = JSON.parse(warehouseStateJSON);
    }

    warehouseState.picturesWarehouse.push(pictureWarehouse);

    warehouseState.currentSize += totalSize;

    warehouseStateJSON = JSON.stringify(warehouseState);

    sessionStorage.setItem('warehouse-state', warehouseStateJSON);
  }

  static getCurrentSize() {
    let currentSize: number = 0;

    let warehouseStateJSON = sessionStorage.getItem('warehouse-state');

    if (warehouseStateJSON !== null) {
      currentSize = JSON.parse(warehouseStateJSON).currentSize;
    }

    return currentSize;
  }

  static getAllPictures() {
    let picturesWarehouse: PictureWarehouse[] = [];

    let warehouseStateJSON = sessionStorage.getItem('warehouse-state');

    if (warehouseStateJSON !== null) {
      picturesWarehouse = JSON.parse(warehouseStateJSON).picturesWarehouse;
    }

    return picturesWarehouse;
  }

  static remove(id: string) {
    let warehouseState: WarehouseState = { currentSize: 0, picturesWarehouse: [] };

    let warehouseStateJSON = sessionStorage.getItem('warehouse-state');

    if (warehouseStateJSON !== null) {
      warehouseState = JSON.parse(warehouseStateJSON);
    }

    let newPicturesWarehouse: PictureWarehouse[] = [];

    for (let item of warehouseState.picturesWarehouse) {
      if (item.id !== id) {
        newPicturesWarehouse.push(item);
      } else {
        warehouseState.currentSize -= item.count * item.size.valueOf();
      }
    }

    warehouseState.picturesWarehouse = newPicturesWarehouse;

    warehouseStateJSON = JSON.stringify(warehouseState);

    sessionStorage.setItem('warehouse-state', warehouseStateJSON);
  }

  static update(pictureWarehouse: PictureWarehouse, newCount: number) {
    let warehouseState: WarehouseState = { currentSize: 0, picturesWarehouse: [] };

    let warehouseStateJSON = sessionStorage.getItem('warehouse-state');

    if (warehouseStateJSON !== null) {
      warehouseState = JSON.parse(warehouseStateJSON);
    }

    let newPicturesWarehouse: PictureWarehouse[] = [];

    for (let item of warehouseState.picturesWarehouse) {
      if (item.id === pictureWarehouse.id) {
        warehouseState.currentSize -= (item.count - newCount) * item.size.valueOf();
        item.count = newCount;
      }
      newPicturesWarehouse.push(item);
    }

    warehouseState.picturesWarehouse = newPicturesWarehouse;

    warehouseStateJSON = JSON.stringify(warehouseState);

    sessionStorage.setItem('warehouse-state', warehouseStateJSON);
  }

  static deleteAllPictures() {
    sessionStorage.removeItem('warehouse-state');
  }
}
