import './App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import WorkshopApp from './components/workshop/WorkshopApp';
import WarehouseApp from './components/warehouse/WarehouseApp';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={WorkshopApp}/>
          <Route exact path="/warehouse" component={WarehouseApp}/>
        </Switch>
      </Router>
    </div>

  );
}

export default App;
