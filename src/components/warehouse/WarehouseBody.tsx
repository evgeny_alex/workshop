import React, { useCallback, useEffect, useState } from 'react';
import './WarehouseBody.css';
import WarehouseStateService from './WarehouseStateService';
import WarehouseItem from './WarehouseItem';
import Button from 'react-bootstrap/cjs/Button';
import PictureSell from '../workshop/PictureSell';
import { PictureWarehouse } from '../workshop/PictureWarehouse';

function WarehouseBody() {
  const initStateChecked = useCallback(() => {
    const picturesWarehouse: PictureWarehouse[] = WarehouseStateService.getAllPictures();

    return picturesWarehouse.map((item) => {
      return { picture: item, checked: false, countSell: '' };
    });
  }, []);

  const [checkedPictures, setCheckedPictures] = useState(initStateChecked());

  const onChangeChecked = useCallback(
    (id: string, checked: boolean) => {
      const newCheckedPictures = checkedPictures.map((item) => {
        if (id === item.picture.id) {
          return { picture: item.picture, checked: checked, countSell: item.countSell };
        } else {
          return item;
        }
      });

      setCheckedPictures(newCheckedPictures);
    },
    [checkedPictures, setCheckedPictures]
  );

  const onChangeCount = useCallback(
    (id: string, count: string) => {
      const newCheckedPictures = checkedPictures.map((item) => {
        if (id === item.picture.id) {
          return { picture: item.picture, checked: item.checked, countSell: count };
        } else {
          return item;
        }
      });

      setCheckedPictures(newCheckedPictures);
    },
    [checkedPictures, setCheckedPictures]
  );

  const onClickDelete = useCallback(
    (id: string) => {
      WarehouseStateService.removePicture(id);

      setCheckedPictures(initStateChecked());
    },
    [initStateChecked]
  );

  const onClickSell = useCallback(() => {
    for (let item of checkedPictures) {
      if (
        (!parseInt(item.countSell) || parseInt(item.countSell) < 0 || parseInt(item.countSell) > item.picture.count) &&
        item.checked
      ) {
        alert('Wrong amount');
        return;
      }
    }

    if (checkedPictures.filter((item) => item.checked).length === 0) {
      alert('No selected pictures.');
      return;
    }

    const pictureToSell: PictureSell[] = checkedPictures.filter((item) => {
      return item.checked;
    });

    WarehouseStateService.sellPictures(pictureToSell);

    if (checkedPictures.length !== 0) {
      alert('The pictures sold successfully.');
    }

    setCheckedPictures(initStateChecked());
  }, [checkedPictures, setCheckedPictures, initStateChecked]);

  useEffect(() => {
    if (checkedPictures.length === 0) {
      alert('The warehouse is empty. Start drawing to fill it!');
    }
  }, [checkedPictures]);

  window.onload = function () {
    WarehouseStateService.deleteAllPictures();
    setCheckedPictures(initStateChecked());
  };

  return (
    <div>
      <div className="Grid-container">
        {checkedPictures.map((item) => (
          <WarehouseItem
            key={item.picture.id}
            id={item.picture.id}
            name={item.picture.name}
            count={item.picture.count}
            onClickDelete={onClickDelete}
            url={item.picture.url}
            onChangeChecked={onChangeChecked}
            checked={item.checked}
            onChangeCount={onChangeCount}
            countSell={item.countSell}
          />
        ))}
      </div>
      <Button className="Button-sell" variant="secondary" size="lg" onClick={onClickSell}>
        Sell
      </Button>
    </div>
  );
}

export default WarehouseBody;
