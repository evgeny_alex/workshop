import React from 'react';

import './WarehouseApp.css';
import WarehouseBody from './WarehouseBody';
import AppHeader from '../workshop/AppHeader';

function WarehouseApp() {
  return (
    <div>
      <AppHeader title="Warehouse" to="/" position="left" textButton="to the workshop" />
      <WarehouseBody />
    </div>
  );
}

export default WarehouseApp;
