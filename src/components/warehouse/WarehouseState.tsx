import { PictureWarehouse } from '../workshop/PictureWarehouse';

export default interface WarehouseState {
  currentSize: number;
  picturesWarehouse: PictureWarehouse[];
}
