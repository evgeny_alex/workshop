import React from 'react';

import './WarehouseItem.css';
import PictureService from '../workshop/PictureService';
import Form from 'react-bootstrap/cjs/Form';

interface WarehouseItemProps {
  id: string;
  name: string;
  count: number;
  url: string;
  checked: boolean;
  countSell: string;
  onClickDelete: (id: string) => void;
  onChangeChecked: (id: string, checked: boolean) => void;
  onChangeCount: (id: string, count: string) => void;
}

function WarehouseItem(props: WarehouseItemProps) {
  return (
    <div className="Item">
      <h4>{props.name}</h4>
      <div className="Image-container">
        <img className="Image-container-image" src={PictureService.getPictureByURL(props.url)} alt="item.url" />
        <button className="Image-container-button-delete" onClick={() => props.onClickDelete(props.id)}>
          X
        </button>
        <div className="Image-container-text">{props.count}</div>
      </div>
      <div className="Field-container">
        <input
          type="checkbox"
          id={props.id}
          className="Field-container-item Input-check"
          checked={props.checked}
          onChange={(event) => {
            props.onChangeChecked(props.id, event.target.checked);
          }}
        />
        <Form.Control
          className="Field-container-item Input-field"
          value={props.countSell}
          onChange={(event) => props.onChangeCount(props.id, event.target.value)}
        />
      </div>
    </div>
  );
}

export default WarehouseItem;
