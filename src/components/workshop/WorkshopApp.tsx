import React from 'react';

import WorkshopBody from './WorkshopBody';
import AppHeader from './AppHeader';

function WorkshopApp() {
  return (
    <div>
      <AppHeader title="Workshop" to="/warehouse" position="right" textButton="to the gallery"/>
      <WorkshopBody/>
    </div>
  );
}

export default WorkshopApp;
