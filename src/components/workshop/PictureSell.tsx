import { PictureWarehouse } from './PictureWarehouse';

export default interface PictureSell {
  picture: PictureWarehouse;
  countSell: string;
}
