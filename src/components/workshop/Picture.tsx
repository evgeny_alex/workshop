import { SizeEnum } from '../../enum/SizeEnum';

export interface Picture {
  genre: string;
  id: string;
  size: SizeEnum;
  tech: string;
  url: string;
}
