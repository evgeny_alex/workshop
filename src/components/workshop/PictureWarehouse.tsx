import { Picture } from './Picture';

export interface PictureWarehouse extends Picture {
  count: number,
  name: string,
}
