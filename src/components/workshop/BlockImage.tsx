import React from 'react';
import './BlockImage.css';
import PictureService from './PictureService';

function BlockImage() {
  return <img className="BlockImage" src={PictureService.getPictureByURL('canvas.jpg')} alt="canvas" />;
}

export default BlockImage;
