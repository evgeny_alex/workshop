import React from 'react';

import './AppHeader.css';
import { Link } from 'react-router-dom';
import Form from 'react-bootstrap/cjs/Form';
import arrow from '../../resources/arrow.png';

interface AppHeaderProps {
  position: string;
  textButton: string;
  title: string;
  to: string;
}

function AppHeader(props: AppHeaderProps) {
  return (
    <div className="AppHeader">
      <Form.Label className="AppHeader-title">{props.title}</Form.Label>
      <Link to={props.to}>
        <button className={'AppHeader-button btn-' + props.position}>
          {props.textButton}
          <img className={'AppHeader-image img-' + props.position} src={arrow} alt="arrow" />
        </button>
      </Link>
    </div>
  );
}

export default AppHeader;
