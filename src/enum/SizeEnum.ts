export enum SizeEnum {

  S = 3,
  M = 4,
  L = 6
}

export class SizeService {

  static getSizeByName(name: string) {
    switch (name) {
      case 'S':
        return SizeEnum.S;
      case 'M':
        return SizeEnum.M;
      default:
        return SizeEnum.L;
    }
  }

}
